const pool = require('../../database/connection')
const bcrypt = require('bcrypt')
const siteModel = require('../../model/siteModel')

class SiteController {
    index(req, res) {
        return res.redirect('/user/login')
    }
    homeFunction(req, res) {
        return res.render('home', {title: "Trang chủ"})
    }
}
module.exports = new SiteController;