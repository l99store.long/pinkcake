const fs = require('fs')
const pdf = require('pdf-creator-node')
const path = require('path')
const options = require('../helpers/optionPdf')

class GeneratePdfController {
    async generatePdf (req, res, next) {
        const data = {name:"Shyam"}
        const html = fs.readFileSync(path.join(__dirname, '../../resources/views/products/generatePdfCart.html'), 'utf-8')
        const fileName = Math.random() + '_doc' + '.pdf'
        const document = {
            html: html,
            data: {name:"Shyam"},
            path: './src/public/docs/' + fileName
        }
        pdf.create(document, options)
            .then(res => {
                console.log('success');
            })
            .catch(err => {
                console.error(err)
            })
        const filePath = 'http://localhost:3000/docs/' + fileName
        res.contentType("application/pdf");
        res.send(filePath)
        var stream = fs.readStream('./src/public/docs/');
        filename = encodeURIComponent(filename);
        res.setHeader('Content-disposition', 'inline; filename="' + filename + '"');
        res.setHeader('Content-type', 'application/pdf');
      
        stream.pipe(res);
        // if (fs.existsSync(filePath)) {
        //     res.contentType("application/pdf");
        //     fs.createReadStream(filePath).pipe(res)
        // }
    }
}

module.exports = new GeneratePdfController
