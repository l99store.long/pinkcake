const pool = require('../../database/connection')

class CustomerController {
    async dashboard(req, res) {
        const allCustomers = await pool.query(`SELECT id, name, phone, address, type, email FROM customers ORDER BY id DESC`)
        const result = allCustomers.rows
        return res.render('customers/dashboard', {result, title: "Trang quản lý khách hàng"})
    }
    async getACustomer(req, res) {
        const ACustomer = await pool.query(`SELECT id, name, phone, address, email, create_datetime FROM customers WHERE id = $1`, [req.params.id])
        const result = ACustomer.rows
        return res.render('customers/update', {result, title: "Trang chỉnh sửa thông tin khách hàng"})
    }
    async createCustomer(req, res) {
        const currentDate = new Date().toLocaleString()
        try {
            const {name, phone, address, email} = req.body
            const createCustomer = await pool.query(`INSERT INTO customers (name, phone, address, email, create_datetime, update_datetime) VALUES ($1,$2,$3,$4,$5,$6)`, 
            [name, phone, address, email, currentDate, null])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/customer')
    }
    async updateCustomer(req, res) {
        const currentDate = new Date().toLocaleString()
        try {
            const {name, phone, address, email} = req.body
            const updateCustomer = await pool.query(`UPDATE customers SET (name, phone, address, email, update_datetime) = ($1,$2,$3,$4,$5) WHERE id = $6`, 
            [name, phone, address, email, currentDate, req.params.id])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/customer')
    }
    async deleteCustomer(req, res) {
        try {
            const delecteCustomer = await pool.query(`DELETE FROM customers WHERE id = $1`, 
            [req.params.id])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/customer')
    }
    getViewCreateCustomer(req, res) {
        return res.render('customers/create', {title: "Trang thêm mới thông tin khách hàng"})
    }
}
module.exports = new CustomerController