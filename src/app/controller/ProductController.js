const pool = require('../../database/connection')
var moment = require('moment');
class ProductController {
    async dashboard(req, res) {
        const allProducts = await pool.query(`
        SELECT products.id, products.name, products.price, products.quantity, products.price_retail, products.profit, categories.name as category
        FROM products 
        INNER JOIN categories 
            ON categories.id = products.category_id
        ORDER BY products.name ASC`)
        const result = allProducts.rows
        return res.render('products/dashboard', {result, title: "Trang quản lý sản phẩm"})
    }
    async getViewCreateProduct(req, res) {
        const allCategories = await pool.query(`SELECT id, name FROM categories`)
        const resultCategories = allCategories.rows
        return res.render('products/create', {resultCategories, title: "Trang thêm mới sản phẩm"})
    }
    async createProduct(req, res) {
        var currentDate = moment().format('YYYY/MM/DD/ HH:mm:ss')
        try {
            const profit = (req.body.price_retail - req.body.price)
            const {name, price, quantity, price_retail, category_id} = req.body
            const createProduct = await pool.query(`INSERT INTO products (name, price, quantity, price_retail, profit, category_id, create_datetime, update_datetime) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`, 
            [name, price, quantity, price_retail, profit, category_id, currentDate, null])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/product')
    }
    async getAProduct(req, res) {
        const AProduct = await pool.query(`
        SELECT products.id, products.name, products.price, products.quantity, products.price_retail, products.profit, products.category_id, categories.name as category
        FROM products 
        INNER JOIN categories 
            ON categories.id = products.category_id 
        WHERE products.id = $1`, [req.params.id])
        
        const allCategories = await pool.query(`
        SELECT id, name 
        FROM categories 
        WHERE id 
            NOT IN (SELECT products.category_id FROM products 
                    INNER JOIN categories 
                    ON categories.id = products.category_id 
                WHERE products.id = $1)`, [req.params.id])
        const result = AProduct.rows
        const resultCategories = allCategories.rows
        return res.render('products/update', {result, resultCategories, title: "Trang chỉnh sửa sản phẩm"})
    }
    async updateProduct(req, res) {
        var currentDate = moment().format('YYYY/MM/DD/ HH:mm:ss')
        try {
            const profit = (req.body.price_retail - req.body.price)
            const {name, price, quantity, price_retail, category_id} = req.body
            const updateProduct = await pool.query(`UPDATE products SET (name, price, quantity, price_retail, profit, category_id, update_datetime) = ($1,$2,$3,$4,$5,$6,$7) WHERE id = $8`, 
            [name, price, quantity, price_retail, profit, category_id, currentDate, req.params.id])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/product')
    }
    async deleteProduct(req, res) {
        try {
            const deleteProduct = await pool.query(`DELETE FROM products WHERE id = $1`, 
            [req.params.id])
        } catch (error) {
            console.error(error.message)
        }
        return res.redirect('/product')
    }
    async getViewCart(req, res) {
        const allUsers = await pool.query('SELECT id, name FROM customers')
        const resultUsers = allUsers.rows
        return res.render('products/cart', {resultUsers, title: "Trang giỏ hàng"})
    }
    async payment(req, res) {
        var currentDate = moment().format('YYYY/MM/DD HH:mm:ss')
        var makeSeqOrders = `ORD-${moment().format('YYYYMMDD-hhmmssSSS-A')}`
        var makeSeqBills = `BIL-${moment().format('YYYYMMDD-hhmmssSSS-A')}`
        var makeTotalPayment = req.body.totalPrice.replace(/\s*,\s*/g, '')
        const {type, customerId, cartPayment} = req.body
        try {
            cartPayment.forEach(objItem => {
                var productId = objItem.productId
                var quantity = objItem.quantity
                const createOrders = pool.query(`INSERT INTO orders (orders_seq, product_id, quantity, create_datetime, update_datetime) VALUES ($1, $2, $3, $4, $5)`, [makeSeqOrders, productId, quantity, currentDate, null])
                const quantityOld = pool.query(`SELECT quantity FROM products WHERE id = $1`, [productId], (err, result) => {
                        if (err) {
                            console.error(err);
                        }
                        var obj = Object.assign({}, ...result.rows)
                        var currentQuantity = Number(obj.quantity) - quantity
                        const updateQuantity = pool.query(`UPDATE products SET (quantity, update_datetime) = ($1, $2) WHERE id = $3`, [currentQuantity, currentDate, productId])
                    })
            });
            const createBills = await pool.query(`
                INSERT INTO bills (bill_seq, orders_seq, customer_id, total_price, type, create_datetime, update_datetime) 
                VALUES ($1, $2, $3, $4, $5, $6, $7)
                `, 
            [makeSeqBills, makeSeqOrders, customerId, makeTotalPayment, type, currentDate, null])
            return res.json(createBills.rows)
        } catch (error) {
            console.error(error.message);
        }
    }
    async search(req, res) {
        const query = req.query.search
        const search = await pool.query(`
            SELECT products.id, products.name, products.price, products.quantity, products.price_retail, products.profit, categories.name as category
            FROM products 
            INNER JOIN categories 
                ON categories.id = products.category_id
            WHERE products.name ILIKE $1
            ORDER BY id ASC`, [`%${query}%`])
        const result = search.rows
        if (result.length == 0) {
            return res.render('products/dashboard', {message: "Không tìm thấy sản phẩm này!", query: query, title: "Trang quản lý sản phẩm"})
        }
        return res.render('products/dashboard', {result, query: query, title: "Trang quản lý sản phẩm"})
    }
}
module.exports = new ProductController