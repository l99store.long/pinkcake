const pool = require('../../database/connection')
const bcrypt = require('bcrypt')
class UserController {
    dashboard(req, res) {
        return res.render('dashboardUser')
    }
    async registerFunction(req, res) {
        const saltRounds = 10;
        const currentDate = new Date().toLocaleString()
        try {
            const {name_display, email, address, number_phone, color, image, type, birth_date, gender} = req.body
            const hashedPassword = bcrypt.hashSync(req.body.password, saltRounds)
            const newUser = await pool.query(`INSERT INTO users (name_display, password, email, gender, birth_date, number_phone, type, address, color, image, create_datetime, update_datetime) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`,
            [name_display, hashedPassword, email, gender, birth_date, number_phone, type, address, color, image, currentDate, null])
            res.json(newUser,rows[0])
        } catch (error) {
            console.error(error.message)
        }
        return res.render('users/register', {layout: false})
    }
    getViewLogin(req, res) {
        return res.render('users/login', {layout: false})
    }
    loginFunction(req, res) {
        res.redirect('/home')
    }
    logoutFunction(req, res) {
        req.logOut()
        req.flash("success_msg", "Logout Success")
        res.redirect('/user/login')
    }
}

module.exports = new UserController;