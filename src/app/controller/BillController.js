const pool = require('../../database/connection')
var moment = require('moment');
class BillController {
    async getViewBills(req, res) {
        var currentDate = moment().format('MM/YYYY')
        var currentMonth = moment().format('MM')
        const allBills = await pool.query(
        `
            SELECT
                bills.bill_seq, 
                customers.name, 
                customers.phone, 
                bills.total_price,
                bills.type,
                COALESCE(to_char(bills.create_datetime, 'DD/MM/YYYY HH24:MI:SS'),'')
            FROM bills
            LEFT JOIN customers
                ON customers.id = bills.customer_id
            WHERE EXTRACT(MONTH FROM bills.create_datetime) = $1
            ORDER BY coalesce DESC
        `, [currentMonth]
        )
        const result = allBills.rows
        var totalOfBillInMonth = 0
        result.forEach(function(element) {
            totalOfBillInMonth += Number(element.total_price)
        })
        return res.render('bills/dashboard', {result, currentDate: currentDate, totalOfBillInMonth,  title: "Trang quản lý hóa đơn"})
    }
    async getViewBillDetail(req, res) {
        const billDetail = await pool.query(
        `
            SELECT 
                bills.bill_seq, 
                customers.name as name_customer, 
                customers.phone, 
                customers.address, 
                products.name as name_product, 
                products.price_retail,
                orders.quantity,
                bills.total_price,
                bills.type,
                COALESCE(to_char(bills.create_datetime, 'MM/DD/YYYY HH24:MI:SS'),'')
            FROM bills
            LEFT JOIN customers
                ON customers.id = bills.customer_id
            LEFT JOIN orders
                ON orders.orders_seq = bills.orders_seq
            LEFT JOIN products
                ON orders.product_id = products.id
            WHERE bill_seq = $1
        `,[req.params.seq])
        return res.json(billDetail.rows)
    }
    async updatePaid(req, res) {
        const type = 'paid'
        const typeBill = await pool.query('UPDATE bills SET type = $1 WHERE bill_seq = $2 RETURNING*', [type, req.params.seq])
        return res.json(typeBill.rows)
    }
    async viewBillOfCustomer(req, res) {
        try {
            const name = await pool.query('SELECT name FROM customers WHERE id = $1', [req.params.id])
            const billOfCustomer = await pool.query(
                `
                SELECT
                    bills.bill_seq, 
                    customers.name, 
                    customers.phone, 
                    bills.total_price,
                    bills.type,
                    COALESCE(to_char(bills.create_datetime, 'MM/DD/YYYY HH24:MI:SS'),'')
                FROM bills
                LEFT JOIN customers
                    ON customers.id = bills.customer_id
                WHERE customers.id = $1
                ORDER BY coalesce DESC
                `,[req.params.id])
                const result = billOfCustomer.rows
                const nameCustomer = name.rows
                if (result.length == 0) {
                    return res.render('bills/searchBill', {message: "Khách hàng này hiện không có hóa đơn mua hàng!", nameCustomer, title: "Trang tìm kiếm hóa đơn"})
                }
                return res.render('bills/searchBill', {result, nameCustomer, title: "Trang tìm kiếm hóa đơn"})
        } catch (error) {
            req.status(403)
        }
        
    }
}
module.exports = new BillController