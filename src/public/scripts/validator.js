function Validator(options) {
    var selectorRules = {}
    var formValidate = document.querySelector(options.formValidate)
    if (formValidate) {
        formValidate.onsubmit = function(e) {
            e.preventDefault()
            var isFormValid = true
            options.rules.forEach(function (rule) {
                var inputValidate = formValidate.querySelector(rule.selector)
                var isValid = validate(inputValidate, rule)
                if (!isValid) {
                    isFormValid = false
                }
            })
            // Check form invalid when submit form
            if (isFormValid) {
                if (typeof options.onSubmit === 'function') {
                    var invalidInputs = formValidate.querySelectorAll('[name]:not([disable]')
                    var formValues = Array.from(invalidInputs).reduce(function(values, input) {
                        switch(input.type) {
                            case 'checkbox':
                                if(!input.matches(':checked')) {
                                    values[input.name] = ''
                                    return values
                                }
                                if(!Array.isArray(values[input.name])) {
                                    values[input.name] = []
                                }
                                values[input.name].push(input.value)
                                break
                            case 'radio':
                                values[input.name] = formValidate.querySelector('input[name="' + input.name + '"]:checked').value
                                break
                            case 'file':
                                values[input.name] = input.files
                                break
                            default:
                                values[input.name] = input.value
                                break
                        }
                        return values
                    }, {})
                    options.onSubmit(formValues)
                } else {
                    // formValidate.onsubmit()
                }
            }
        }
    }
    // Get form validate
    options.rules.forEach(function (rule) {
        var inputValidate = formValidate.querySelectorAll(rule.selector)
        if (Array.isArray(selectorRules[rule.selector])) {
            selectorRules[rule.selector].push(rule.testers)
        } else {
            selectorRules[rule.selector] = [rule.testers]
        }
        Array.from(inputValidate).forEach(function(inputValidate) {
            // User onBlur
            inputValidate.onblur = function() {
                validate(inputValidate, rule)
            }
            // User onInput
            inputValidate.oninput = function() {
                var errorElement = getParent(inputValidate, options.formGroupSelector).querySelector(options.errorSelector)
                errorElement.innerText = ''
                getParent(inputValidate, options.formGroupSelector).classList.remove('invalid')
            }
        })
    });
    function getParent(element, selector) {
        while(element.parentElement) {
            if(element.parentElement.matches(selector)) {
                return element.parentElement
            }
            element = element.parentElement
        }
    }
    function validate(inputValidate, rule) {
        var errorElement = getParent(inputValidate, options.formGroupSelector).querySelector(options.errorSelector)
        var rules = selectorRules[rule.selector]
        var errorMsg
        for(var i = 0; i < rules.length; ++i) {
            switch(inputValidate.type) {
                case 'checkbox':
                case 'radio':
                    errorMsg = rules[i](formValidate.querySelector(rule.selector + ':checked'))
                    break
                default:
                    errorMsg = rules[i](inputValidate.value)
            }
            if (errorMsg) break
        }
        if (errorMsg) {
            errorElement.innerText = errorMsg
            getParent(inputValidate, options.formGroupSelector).classList.add('invalid')
        } else {
            errorElement.innerText = ''
            getParent(inputValidate, options.formGroupSelector).classList.remove('invalid')
        }
        return !errorMsg
    }
}

Validator.isRequired = function(selector, message) {
    return {
        selector: selector,
        testers: function(value) {
            return value ? undefined : message
        }
    }
}

Validator.isEmail = function(selector, message) {
    return {
        selector: selector,
        testers: function(value) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return regex.test(value) ? undefined : message
        }
    }
}

Validator.isMinLength = function(selector, min, message) {
    return {
        selector: selector,
        testers: function(value) {
            return value.length >= min ? undefined : message
        }
    }
}

Validator.isMaxLength = function(selector, max, message) {
    return {
        selector: selector,
        testers: function(value) {
            return value.length <= max ? undefined : message
        }
    }
}

Validator.isConfirmed = function(selector, getConfirmValue, message) {
    return {
        selector: selector,
        testers: function(value) {
            return value === getConfirmValue() ? undefined : message
        }
    }
}
