Validator({
    formValidate: '#form_register',
    formGroupSelector: '.form-group',
    errorSelector: ".form-message", 
    rules: [
        Validator.isRequired('#name_login', message.Msg_001),
        Validator.isRequired('#name_display', message.Msg_001),
        Validator.isRequired('input[name="gender"]', message.Msg_001),
        Validator.isRequired('#province', message.Msg_001),
        Validator.isRequired('#email', message.Msg_001),
        Validator.isEmail('#email', 'Day khong phai la email'),
        Validator.isRequired('#password', message.Msg_001),
        Validator.isMinLength('#password', 8, 'Vui long nhap toi thieu 8 ky tu'),
        Validator.isRequired('#password_confirm', message.Msg_001),
        Validator.isConfirmed('#password_confirm', function () {
            return document.querySelector('#form_register #password').value
        }, 'Mat khau ko chinh xac'),
        Validator.isRequired('#image', message.Msg_001)
    ],
    onSubmit: async function(data) {
        console.log(data);
        const response = await fetch('/user/register', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        });
        return response.json()
        // .then(response => response.json())
        // .then(data => {
        // console.log('Success:', data);
        // })
        // .catch((error) => {
        // console.error('Error:', error);
        // });
    }
})