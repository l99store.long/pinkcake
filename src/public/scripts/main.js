(function () {
  'use strict'

    var closeSidebar = document.getElementById('close-sidebar')
    var showSidebar = document.getElementById('show-sidebar')
    var myModal = document.getElementById('staticBackdrop')
    var myInput = document.getElementById('name')
    // var isValidated = document.getElementsByClassName('was-validated')
    // var bodyModal = document.getElementsByClassName('modal-body')
    // var titleModal = document.getElementsByClassName('modal-title')
    // form need handle in modal
    // var formCreate = document.getElementById('form_create_customer')
    // var formUpdate = document.getElementById('form_update_customer')
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
    // var btnGenerate = document.getElementsByClassName('btn-generate')[0]
    // btnGenerate.addEventListener('click', GeneratePdf)

    function GeneratePdf() {
      // window.open('/generatePdf','_blank')
      fetch('/generatePdf')
        .then(res => res.json())
        .then(data => {
          console.log(data)
          // window.open(data,'_blank')
        })
        .catch(err => console.error(err))
    }
    // myModal.addEventListener('shown.bs.modal', function (e) {
    //   console.error(e.relatedTarget);
    //   if (e.relatedTarget.classList.contains('btnCreateCustomer')){
    //     formCreate.style.display = 'block'
    //     titleModal[0].innerText = 'Create customer'
    //     bodyModal[0].append(formCreate)
    //   } else if (e.relatedTarget.classList.contains('btnUpdateCustomer')){
    //     formUpdate.style.display = 'block'
    //     titleModal[0].innerText = 'Update customer'
    //     bodyModal[0].append(formUpdate)
    //   }
    // })
    // myModal.addEventListener('hidden.bs.modal', function (e) {
    //   if (isValidated.length > 0){
    //     isValidated[0].classList.remove('was-validated')
    //   }
    //   if (bodyModal[0].hasChildNodes()) {
    //     bodyModal[0].removeChild(bodyModal[0].children[0]);
    //   }
    // });
    
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      })
    closeSidebar.onclick = function closeSidebar() {
      document.getElementById("page-wrapper").classList.remove("toggled");
    }
    showSidebar.onclick = function showSidebar() {
      document.getElementById("page-wrapper").classList.add("toggled");
    }
  })()
// jQuery(function ($) {
//     $(".sidebar-dropdown > a").click(function() {
//         $(".sidebar-submenu").slideUp(200);
//         if (
//             $(this)
//             .parent()
//             .hasClass("active")
//         ) {
//             $(".sidebar-dropdown").removeClass("active");
//             $(this)
//             .parent()
//             .removeClass("active");
//         } else {
//             $(".sidebar-dropdown").removeClass("active");
//             $(this)
//             .next(".sidebar-submenu")
//             .slideDown(200);
//             $(this)
//             .parent()
//             .addClass("active");
//         }
//     });

// $("#close-sidebar").click(function() {
//   $(".page-wrapper").removeClass("toggled");
// });
// $("#show-sidebar").click(function() {
//   $(".page-wrapper").addClass("toggled");
// });


