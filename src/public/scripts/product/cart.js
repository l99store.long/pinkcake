(function () {
    'use strict'

    if (document.readyState == 'loading') {
        document.addEventListener('DOMContentLoaded', ready)
    } else {
        ready()
    }

    function ready() {
        if (document.getElementsByClassName('cart').length > 0) {
            initCartItem()
            var btnDeleteItem = document.getElementsByClassName('btn-delete-item')
            for (var i = 0; i < btnDeleteItem.length; i++) {
                var btnCurrent = btnDeleteItem[i]
                btnCurrent.addEventListener('click', deleteItem)
            }

            var itemQuantity = document.getElementsByClassName('item-quantity')
            for (var i = 0; i < itemQuantity.length; i++) {
                var quantity = itemQuantity[i]
                quantity.addEventListener('change', quantityChanged)
            }
            var btnDeleteAllItem = document.getElementsByClassName('btn-delete-all-item')
            for (var i = 0; i < btnDeleteAllItem.length; i++) {
                var btnDelete = btnDeleteAllItem[i]
                btnDelete.addEventListener('click', deleteAllItem)
            }

            var btnPaid = document.getElementsByClassName('btn-paid')[0]
            btnPaid.addEventListener('click', paymentCart)

            var btnDebt = document.getElementsByClassName('btn-debt')[0]
            btnDebt.addEventListener('click', paymentCart)
        }
        // Add item to cart
        var btnAddItems = document.getElementsByClassName('btn-add-item')
        for (var i = 0; i < btnAddItems.length; i++) {
            var addItem = btnAddItems[i]
            addItem.addEventListener('click', addItemsClicked)
        }

        var billType = document.getElementsByClassName('bill-type')
        for (let i = 0; i < billType.length; i++) {
            const element = billType[i];
            if (element.innerText == 'paid') {
                element.classList.add('paid')
                element.innerText = 'Đã thanh toán'
            } else if (element.innerText == 'debt') {
                element.classList.add('debt')
                element.innerText = 'Công nợ'
            }
        }
        var trBill = document.getElementsByClassName('tr-bill')
        for (let i = 0; i < trBill.length; i++) {
            var trClicked = trBill[i];
            trClicked.addEventListener('click', showBillDetail)
        }

        var btnViewCart = document.getElementsByClassName('btn-view-cart')
        if (btnViewCart.length > 0) {
            btnViewCart[0].addEventListener('click', getViewCart)
        }

        var btnViewInsert = document.getElementsByClassName('btn-add-product')
        if (btnViewInsert.length > 0) {
            btnViewInsert[0].addEventListener('click', getViewInsertProduct)
        }

        var inputNumber = document.querySelectorAll('input[type="number"]')
        for (let i = 0; i < inputNumber.length; i++) {
            const input = inputNumber[i];
            input.addEventListener('input', numberWithCommas)
        }

        var btnViewInsertCustomer = document.getElementsByClassName('btn-add-customer')
        if (btnViewInsertCustomer.length > 0) {
            btnViewInsertCustomer[0].addEventListener('click', getViewInsertCustomer)
        }
        var formSelectCustomer = document.getElementsByClassName('form-select-customer')
        if (formSelectCustomer.length > 0) {
            formSelectCustomer[0].addEventListener('change', onChangeCustomerName)
        }
    }
    
    // Load item for cart
    function initCartItem() {
        var itemTotalPrice = document.getElementsByClassName('item-total-price')[0]
        var cartLength = document.getElementsByClassName('cart-length')[0]
        var cartMain = document.getElementsByClassName('cart-main')[0]
        if (sessionStorage.products !== undefined) {
            var sessionItem = JSON.parse(sessionStorage.products)
            var total = 0
            for (var i = 0; i < sessionItem.length; i++) {
                var item = sessionItem[i];
                total = total + (item.productPrice * item.quantity)
                var cartRow = document.createElement('div')
                cartRow.classList.add('cart-row', 'row', 'main')
                var cartRowContent = 
                `
                    <input type="hidden" class="item-id" value="${item.productId}">
                    <div class="col">
                        <div class="row item-name">${item.productName}</div>
                    </div>
                    <div class="col col-quantity">
                        <input class="item-quantity" type="number" value="${item.quantity}">
                    </div>
                    <div class="col col-price">
                        <label class="item-price">${formatThousandNumber(item.productPrice)}</label>
                    </div>
                    <div class="col col-delete">
                        <i style="cursor: pointer;" class="fas fa-trash btn-delete-item"></i>
                    </div>
                `
                cartRow.innerHTML += cartRowContent
                cartMain.append(cartRow)
            }
            itemTotalPrice.innerText = formatThousandNumber(total)
            cartLength.innerText = `Hiện có: ${sessionItem.length} sản phẩm`
        } else {
            var btnDisabled = document.querySelectorAll('button')
            for (var i = 0; i < btnDisabled.length; i++) {
                var item = btnDisabled[i];
                item.setAttribute('disabled', '')
            }
            var lengthZero = document.getElementsByClassName('item-length-zero')[0]
            lengthZero.innerText = `Hiện có: 0 sản phẩm`
            var cartHeader = document.getElementsByClassName('cart-header')[0]
            cartHeader.style.display = 'none'
        }
    }

    function deleteItem(e) {
        var btnClicked = e.target
        var id = btnClicked.parentElement.parentElement.getElementsByClassName('item-id')[0].value
        btnClicked.parentElement.parentElement.remove()
        var sessionItem = JSON.parse(sessionStorage.products)
        const index = sessionItem.findIndex(function(obj) {
            return obj.productId == id
          })
          if (index > -1) {
            sessionItem.splice(index, 1);
          }
        sessionStorage.products = JSON.stringify(sessionItem)
        updateCart()
    }

    function quantityChanged(e) {
        var quantity = e.target
        var parent = quantity.parentElement.parentElement
        var id = parent.getElementsByClassName('item-id')[0].value
        var quantityChanged = parent.getElementsByClassName('item-quantity')[0].value
        var sessionItem = JSON.parse(sessionStorage.products)
        sessionItem.map(function(obj) {
            if (obj.productId === id) {
                obj.quantity = Number(quantityChanged)
            }
        })
        sessionStorage.products = JSON.stringify(sessionItem)
        if(isNaN(quantity.value) || quantity.value <= 0) {
            quantity.value = 1
        }
        updateCart()
    }

    function deleteAllItem() {
        var cartMain = document.getElementsByClassName('cart-main')[0]
        while (cartMain.hasChildNodes()) {
            cartMain.removeChild(cartMain.firstChild)
        }
        sessionStorage.clear()
        updateCart()
    }

    function addItemsClicked(e) {
        var btnAdd = e.target
        var listSessionItem = sessionStorage.products !== undefined ? JSON.parse(sessionStorage.products) : []
        var quantity = 1
        var objItemDetail = {}
        var product = btnAdd.parentElement.parentElement.parentElement
        var productId = product.getElementsByClassName('product-id')[0].value
        var productName = product.getElementsByClassName('product-name')[0].innerText
        var productPrice = product.getElementsByClassName('product-price-retail')[0].innerText
        var message = `Thêm ${productName} vào giỏ hàng thành công!`
        objItemDetail.productId = productId
        objItemDetail.productName = productName
        objItemDetail.quantity = quantity
        objItemDetail.productPrice = formatNumeric(productPrice)
        if (listSessionItem.length == 0) {
            listSessionItem.push(objItemDetail)
        } else {
            listSessionItem.forEach(objItem => {
                if (objItem.productId == objItemDetail.productId) {
                    objItem.quantity += 1
                } else {
                    if (!listSessionItem.some(obj => obj.productId == objItemDetail.productId)) {
                        listSessionItem.push(objItemDetail)
                    }
                }
            });
        }

        sessionStorage.products = JSON.stringify(listSessionItem)
        displayAlertSuccess(message)
    }

    function updateCart() {
        var total = 0
        var cartMain = document.getElementsByClassName('cart-main')[0]
        var cartRows = cartMain.getElementsByClassName('cart-row')
        var itemTotalPrice = document.getElementsByClassName('item-total-price')[0]
        // var total = formatNumeric(document.getElementsByClassName('item-total-price')[0].innerText)
        var cartLength = document.getElementsByClassName('cart-length')[0]
        if (cartRows.length == 0) {
            var btnDisabled = document.querySelectorAll('button')
            for (var i = 0; i < btnDisabled.length; i++) {
                var item = btnDisabled[i];
                item.setAttribute('disabled', '')
            }
            var lengthZero = document.getElementsByClassName('item-length-zero')[0]
            lengthZero.innerText = `Hiện có: 0 sản phẩm`
            var cartHeader = document.getElementsByClassName('cart-header')[0]
            cartHeader.style.display = 'none'
        }
        for (var i = 0; i < cartRows.length; i++) {
            var cartRow = cartRows[i]
            var itemPrice = cartRow.getElementsByClassName('item-price')[0]
            var itemQuantity = cartRow.getElementsByClassName('item-quantity')[0]

            var price = formatNumeric(itemPrice.innerText)
            var quantity = formatNumeric(itemQuantity.value)
            total = total + (price * quantity)
        }
        itemTotalPrice.innerText = formatThousandNumber(total)
        cartLength.innerText = `Hiện có: ${cartRows.length} sản phẩm`
    }

    function formatThousandNumber(value) {
        return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }

    function formatNumeric(value) {
        var makeNumber = value.replace(/\s*,\s*/g, '')
        return Number(makeNumber)
    }

    function paymentCart(e) {
        var btnPayment = e.target
        var type = null
        if (btnPayment.classList.contains('btn-paid')) {
            type = 'paid'
        } else if (btnPayment.classList.contains('btn-debt')) {
            type = 'debt'
        }
        var totalPrice = btnPayment.parentElement.parentElement.getElementsByClassName('item-total-price')[0].innerText
        var customerId = btnPayment.parentElement.parentElement.getElementsByClassName('customer-id-real')[0].value
        var cartPayment = JSON.parse(sessionStorage.products)
        fetch('/product/payment', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({type, totalPrice, customerId, cartPayment}),
        })
        .then(response => response.json())
        .then(data => {
            var message = 'Thanh toán hóa đơn thành công!'
            displayAlertSuccess(message)
            sessionStorage.clear()
            setTimeout(function() {
                window.location.replace('/bill')
            },2000)
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    function showBillDetail(e) {
        var parentClicked = e.target.parentElement
        var billSeq = parentClicked.getElementsByClassName('bill-seq')[0].innerText
        fetch(`/bill/detail/${billSeq}`, {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(response => response.json())
        .then(data => {
            var billDetailModal = new bootstrap.Modal(document.getElementById('bill-detail-modal'))
            var billMain = document.getElementsByClassName('bill-main')[0]
            while (billMain.hasChildNodes()) {
                billMain.removeChild(billMain.firstChild)
            }

            var modalFooter = document.getElementsByClassName('modal-footer')[0]
            if (modalFooter.hasChildNodes()) {
                var btn = document.getElementsByClassName('btn-payment')[0]
                if (btn != undefined) {
                    modalFooter.removeChild(btn) 
                }
            }
            var billSeq = document.getElementsByClassName('bill-seq-detail')[0]
            var billCreated = document.getElementsByClassName('bill-created')[0]
            var billType = document.getElementsByClassName('bill-type-detail')[0]
            var billTotal = document.getElementsByClassName('bill-total')[0]
            var cusName = document.getElementsByClassName('customer-name')[0]
            var cusPhone = document.getElementsByClassName('customer-phone')[0]
            var cusAddress = document.getElementsByClassName('customer-address')[0]
            for (var i = 0; i < data.length; i++) {
                var obj = data[i];
                billSeq.innerText = obj.bill_seq
                billCreated.innerText = obj.coalesce
                cusName.innerText = obj.name_customer
                cusPhone.innerText = obj.phone
                cusAddress.innerText = obj.address
                billTotal.innerText = formatThousandNumber(obj.total_price)
                if (obj.type == 'debt') {
                    // var modalFooter = document.getElementsByClassName('modal-footer')[0]
                    if (modalFooter.getElementsByClassName('btn-payment').length == 0){
                        var btnPayment = document.createElement('button')
                        btnPayment.classList.add('btn', 'btn-payment')
                        btnPayment.innerText = 'Thanh toán'
                        btnPayment.style.color = 'white'
                        btnPayment.onclick = paymentDebt
                        modalFooter.prepend(btnPayment)
                    }
                    if (billType.classList.contains('paid')) {
                        billType.classList.remove('paid')
                    }
                    billType.classList.add('debt')
                    billType.innerText = 'Công nợ'
                } else if (obj.type == 'paid') {
                    if (billType.classList.contains('debt')) {
                        billType.classList.remove('debt')
                    }
                    billType.classList.add('paid')
                    billType.innerText = 'Đã thanh toán'
                }
                var rowBill = document.createElement('div')
                rowBill.classList.add('row', 'main')
                var billRowContent = 
                    `
                        <div class="col">
                            <div class="row">${obj.name_product}</div>
                        </div>
                        <div class="col">
                            <input class="item-quantity" disabled type="number" value="${obj.quantity}">
                        </div>
                        <div class="col">
                            <label class="item-price">${formatThousandNumber(obj.price_retail)}</label>
                        </div>
                    `
                rowBill.innerHTML += billRowContent
                billMain.append(rowBill)
            }
            billDetailModal.show()
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }
    
    function paymentDebt(e) {
        var btnPaymentDebt = e.target.parentElement.parentElement
        var billSeq = btnPaymentDebt.getElementsByClassName('bill-seq-detail')[0].innerText
        fetch(`/bill/update/${billSeq}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(res => res.json())
        .then(data => {
            closeModal('bill-detail-modal')
            var message = `Thanh toán hóa đơn ${billSeq} thành công!`
            displayAlertSuccess(message)
            setTimeout(function() {
                window.location.replace('/bill')
            },2000)
        })
    }

    function getViewCart() {
        window.location = '/product/cart'
    }

    function getViewInsertProduct() {
        window.location = '/product/create'
    }

    function getViewInsertCustomer() {
        window.location = '/customer/create'
    }

    function numberWithCommas(e) {
        // var valueCommas = e.target.value.replace(/,/g, '');
        // if (!valueCommas || valueCommas.endsWith('.')) return;
        // e.target.value = valueCommas.toLocaleString();
    }

    function onChangeCustomerName(e) {
        var customerIdReal = document.getElementsByClassName('customer-id-real')[0]
        var customerNameReal = document.getElementsByClassName('customer-name-real')[0]
        customerIdReal.value = e.target.value
        customerNameReal.value = e.target.options[e.target.selectedIndex].text

    }

    // Handle alert
    function displayAlertSuccess(message) {
        var alertSuccess = document.getElementsByClassName('alert-success')[0]
        alertSuccess.innerText = message
        alertSuccess.style.display = 'block'
        alertSuccess.style.animation = 'fadeIn 0.3s ease-in both'
        setTimeout(function() {
            alertSuccess.style.animation = 'fadeOut 0.5s ease-in both'
        }, 3000)
    }

    // Close modal
    function closeModal(id) {
        var currentModal = document.getElementById(`${id}`)
        var modal = bootstrap.Modal.getInstance(currentModal)
        modal.hide();
    }
})()