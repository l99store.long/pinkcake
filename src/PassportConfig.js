const localStrategy = require('passport-local').Strategy
const pool = require('./database/connection')
const bcrypt = require('bcrypt')
const { use } = require('passport')
const e = require('express')

function initPassport(passport) {
    const authUser = (email, passport, done) => {
        pool.query(`SELECT * FROM users where email = $1`, [email],
        (error, results) => {
            if(error) {
                throw error
            }
            if (results.rows.length > 0) {
                const user = results.rows[0]
                bcrypt.compare(passport, user.password, (error, isMath) => {
                    if (error) {
                        throw error
                    }
                    if (isMath) {
                        return done(null, user)
                    } else {
                        return done(null, false, {message: "Password is correct"})
                    }
                })
            } else {
                return done(null, false, {message: "Name is not registered"})
            }
        })
    }
    passport.use(new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        }, authUser))
    passport.serializeUser((user, done) => done(null, user.id))
    passport.deserializeUser((id, done) => {
        pool.query(`SELECT * FROM users WHERE id = $1`, [id], (error, results) => {
            if(error) {
                throw error
            }
            return done(null, results.rows[0])
        })
    })
}
module.exports = initPassport