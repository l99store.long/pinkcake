DROP TABLE IF EXISTS products

CREATE TABLE IF NOT EXISTS products (
    id bigint GENERATED ALWAYS AS IDENTITY,
    name CHARACTER VARYING NOT NULL,
    price NUMERIC NOT NULL,
    quantity NUMERIC,
    price_retail NUMERIC NOT NULL,
    profit NUMERIC NOT NULL,
    category_id bigint NOT NULL,
    create_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    update_datetime TIMESTAMP WITHOUT TIME ZONE
)

ALTER TABLE products
ADD CONSTRAINT product_id_pk PRIMARY KEY (id)

ALTER TABLE products
ADD CONSTRAINT product_category_fk FOREIGN KEY (category_id) REFERENCES categories(id) 

ALTER TABLE products
ADD CONSTRAINT product_name_uni UNIQUE (name)