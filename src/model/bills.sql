DROP TABLE IF EXISTS bills

CREATE TABLE IF NOT EXISTS bills (
    id bigint GENERATED ALWAYS AS IDENTITY,
    bill_seq CHARACTER VARYING NOT NULL,
    orders_seq CHARACTER VARYING NOT NULL,
    customer_id bigint,
    total_price numeric NOT NULL,
    type character varying NOT NULL,
    create_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    update_datetime TIMESTAMP WITHOUT TIME ZONE
)

ALTER TABLE bills
ADD CONSTRAINT bills_id_pk PRIMARY KEY (id)

ALTER TABLE bills
ADD CONSTRAINT bills_orders_seq_fk FOREIGN KEY (orders_seq) REFERENCES orders(orders_seq) 

ALTER TABLE bills
ADD CONSTRAINT bills_customer_id_fk FOREIGN KEY (customer_id) REFERENCES customers(id) 

ALTER TABLE bills
ADD CONSTRAINT bill_seq_uni UNIQUE (bill_seq)