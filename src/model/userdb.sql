DROP TABLE IF EXISTS users

CREATE TABLE IF NOT EXISTS users (
    id bigint GENERATED ALWAYS AS IDENTITY,
    name_display CHARACTER VARYING(40) NOT NULL,
    password CHARACTER VARYING(255) NOT NULL,
    email CHARACTER VARYING(255) NOT NULL,
    gender smallint NOT NULL,
    birth_date date NOT NULL,
    number_phone CHARACTER VARYING(10),
    type smallint,
    address CHARACTER VARYING(500),
    color CHARACTER VARYING(7),
    image CHARACTER VARYING (512),
    create_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    update_datetime TIMESTAMP WITHOUT TIME ZONE
)

ALTER TABLE users
ADD CONSTRAINT name_login_uni UNIQUE (email);

ALTER TABLE users
ADD CONSTRAINT user_id_fk PRIMARY KEY (id);