DROP TABLE IF EXISTS orders

CREATE TABLE IF NOT EXISTS orders (
    id bigint GENERATED ALWAYS AS IDENTITY,
    orders_seq character varying NOT NULL,
    product_id bigint NOT NULL,
    quantity numeric NOT NULL,
    create_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    update_datetime TIMESTAMP WITHOUT TIME ZONE
)

ALTER TABLE orders
ADD CONSTRAINT orders_id_pk PRIMARY KEY (id)

ALTER TABLE orders
ADD CONSTRAINT orders_seq_uni UNIQUE (orders_seq)