require("dotenv").config()
const express = require('express')
const morgan = require('morgan')
const exphbs = require('express-handlebars')
const path = require('path')
const route = require('./routes')
const bodyParser = require('body-parser');
const session = require('express-session')
const flash = require('express-flash')
const passport = require('passport')
const initPassport = require('./PassportConfig')
const SECRET = process.env.SECRET
const HOST = process.env.PINKCAKE_SERVER_HOST
const PORT = process.env.PINKCAKE_SERVER_PORT || 3000
const app = express()

initPassport(passport)
// Use static
app.use(express.static(path.join(__dirname, 'public')))

// Middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Session
app.use(session({
  secret: SECRET,
  resave: false,
  saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())
// Flash
app.use(flash())
// Use morgan - HTTP Logger
app.use(morgan('combined'))

// Template engine

const hbs = exphbs.create({
  extname: '.hbs',
  helpers: {
    formatThousandNumber: function(value) {
      return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
  }
})

app.engine('hbs', hbs.engine) 
app.set('view engine', 'hbs')
app.set('views', path.join(__dirname, 'resources', 'views'))

app.use(function mainUser(req, res, next) {
  res.locals.user = req.user;
  next();
});

// Use route
route(app)


// Listen port
app.listen(PORT, () => {
    console.log(`Runing on http://${HOST}:${PORT}`)
  })