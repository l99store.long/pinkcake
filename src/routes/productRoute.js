const express = require('express');
const router = express.Router()
const productController = require('../app/controller/ProductController');

router.get('/search', productController.search)
router.post('/payment', productController.payment)
router.get('/cart', productController.getViewCart)
router.get('/delete/:id', productController.deleteProduct)
router.get('/update/:id', productController.getAProduct)
router.post('/update/:id', productController.updateProduct)
router.post('/create', productController.createProduct)
router.get('/create', productController.getViewCreateProduct)
router.get('/', productController.dashboard)

module.exports = router