const express = require('express');
const router = express.Router()
const passport = require('passport');
const userController = require('../app/controller/UserController')

// Check Auth
function checkAuthenticated(req, res, next) {
    if(req.isAuthenticated()) {
      return res.redirect('/home')
    }
    next()
}

function checkNoAuthenticated(req, res, next) {
    if(req.isAuthenticated()) {
      return next()
    }
    res.redirect('/user/login')
}

router.get('/logout', userController.logoutFunction);
router.post('/login', checkAuthenticated, passport.authenticate('local', {
    successRedirect: '/home',
    failureRedirect: '/user/login',
    failureFlash: true
}));
router.get('/login', userController.getViewLogin);
router.post('/register', userController.registerFunction);
router.get('/register', userController.registerFunction);
router.get('/dashboard', checkNoAuthenticated, userController.dashboard)

module.exports = router
