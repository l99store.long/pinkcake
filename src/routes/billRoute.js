const express = require('express');
const router = express.Router()
const billController = require('../app/controller/BillController');

router.get('/customer/:id', billController.viewBillOfCustomer)
router.post('/update/:seq', billController.updatePaid)
router.post('/detail/:seq', billController.getViewBillDetail)
router.get('/', billController.getViewBills)

module.exports = router