const siteRouter = require('./siteRoute')
const usersRouter = require('./usersRoute')
const productRoute = require('./productRoute')
const customerRoute = require('./customerRoute')
const billRoute = require('./billRoute')

function checkNoAuthenticated(req, res, next) {
    if(req.isAuthenticated()) {
      return next()
    }
    res.redirect('/user/login')
}

function routes(app) {
    app.use('/customer', checkNoAuthenticated, customerRoute)
    app.use('/bill', checkNoAuthenticated, billRoute)
    app.use('/product', checkNoAuthenticated, productRoute);
    app.use('/user', usersRouter);
    app.use('/', checkNoAuthenticated, siteRouter);
} 
module.exports = routes;