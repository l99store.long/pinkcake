const express = require('express');
const router = express.Router()
const customerController = require('../app/controller/CustomerController');

router.get('/delete/:id', customerController.deleteCustomer)
router.get('/update/:id', customerController.getACustomer)
router.post('/update/:id', customerController.updateCustomer)
router.post('/create', customerController.createCustomer)
router.get('/create', customerController.getViewCreateCustomer)
router.get('/', customerController.dashboard)

module.exports = router