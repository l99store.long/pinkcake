const express = require('express');
const router = express.Router()
const siteController = require('../app/controller/SiteController');
const generatePdfController = require('../app/controller/GeneratePdfController')

router.get('/generatePdf', generatePdfController.generatePdf)
router.get('/home', siteController.homeFunction);
router.get('/', siteController.index);

module.exports = router;