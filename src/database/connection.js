const {Pool, Client} = require('pg')
require("dotenv").config();
const USER = process.env.PINKCAKE_DATABASE_USER
const PASSWORD = process.env.PINKCAKE_DATABASE_PASSWORD
const HOST = process.env.PINKCAKE_DATABASE_HOST
const PORT = process.env.PINKCAKE_DATABASE_PORT
const DATABASE = process.env.PINKCAKE_DATABASE_NAME


const pool = new Pool({
    user: USER,
    host: HOST,
    password: PASSWORD,
    port: PORT,
    database: DATABASE
});

// pool.query(
//     `CREATE TABLE IF NOT EXISTS users (
//     id bigint SERIAL PRIMARY KEY,
//     name  CHARACTER VARYING(40) NOT NULL,
//     email CHARACTER VARYING(255) NOT NULL,
//     address CHARACTER VARYING(500),
//     password CHARACTER VARYING(255) NOT NULL,
//     number_phone CHARACTER VARYING(10),
//     type smallint,
//     color CHARACTER VARYING(7),
//     create_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
//     update_datetime TIMESTAMP WITHOUT TIME ZONE
// )`, (err, res) => {
//     // console.log(res);
//     pool.end()
// })
module.exports = pool;